'''
Name: Shiho Matsuura
Program: Page Rank Algorithm
'''
class Node:
	def __init__(self, node_info_dict, name):
		self.name = name
		self.edges = []
		self.starting_points = 100
		self.received_points = 0


	def set_edge_nodes(self, vertices_dict, node_info_dict):
		edges = node_info_dict[self.name]
		for edge in edges:
			self.edges.append(vertices_dict[edge])


	def get_edge_nodes(self):
		return self.edges


	def give_out_points(self):
		for destination_node in self.edges:
			destination_node.receive_points(float(self.starting_points) / len(self.edges))
		self.starting_points -= (float(self.starting_points) / len(self.edges)) * len(self.edges)


	def receive_points(self, points):
		self.received_points += float(points)


	def total_points(self):
		final = float(self.received_points) + float(self.starting_points)
		self.starting_points = final
		self.received_points = 0
		return final


def make_all_nodes(vertices, node_info_dict, content):
		Node_dict = {}
		for i in vertices:
			for k, v in node_info_dict.iteritems():
				Node_dict[i] = Node(v, node_info_dict).name
		return Node_dict


def vertex_nodes_list(node_names, node_info_dict):
	#creates instances of vertices
	vertices = []
	for i in node_names:
		vertices.append(Node(node_info_dict, i))
	return  vertices


def make_vertex_nodes(node_ids, node_info_dict, content):
	# uses list of node vertices names to create instances of object Node
	vertices = vertex_nodes_list(node_ids, node_info_dict)
	for a in node_names(content):
		make_all_nodes(vertices, node_info_dict, content)
	return vertices


def vertex_node_dict(vertex_names, vertex_nodes):
	#makes name to actual instance dict
	dict = {}
	for vname in vertex_names:
		for vnode in vertex_nodes:
			if vnode.name == vname:
				dict[vname] = vnode
	return dict


def vertexNode_toedgeNode_dict(vertex_node_dict, node_info_dict):
	dict = {}
	for k, v in node_info_dict.iteritems():
		lst = []
		for i in v:
			lst.append(vertex_node_dict[i])
		dict[vertex_node_dict[k]] = [lst]
	return dict


def contents(filename):
	with open(filename, "r") as f:
		return [x for x in f.read().split('\n') if x]


def node_names(content):
	return [item for item in content[1:int(content[0]) + 1]]


def nodes_and_edges(content):
	return [item.split(" ") for item in content[int(content[0]) + 2:]]


def edge_directions(content):
	# type: (object) -> object
	node_to_edge = {}
	for node, edge in nodes_and_edges(content):
		# checks if the first part of list (node name) already exists in the pointer dictionary
		if node not in node_to_edge.keys():
			node_to_edge[node] = []
		node_to_edge[node].append(edge)
	return node_to_edge


def main():
	filename = raw_input("enter filename: \n")
	content = contents(filename)

	node_ids =  node_names(content)

	print "\n node info dictionary: "
	node_info_dict = edge_directions(content)
	print node_info_dict

	make_vertex_nodes(node_ids, node_info_dict, content)

	vertex_names = make_vertex_nodes(node_ids, node_info_dict, content)

	v_to_n_dict = vertex_node_dict(node_ids, vertex_names)

	full_dict = vertexNode_toedgeNode_dict(v_to_n_dict, node_info_dict)

	for k in full_dict.keys():
		k.set_edge_nodes(v_to_n_dict, node_info_dict)

	for k, v in full_dict.iteritems():
		k.give_out_points()

	print "\nfinal points:"
	for k in full_dict.keys():
		print k.name, k.total_points()


if __name__ == '__main__':
	main()
